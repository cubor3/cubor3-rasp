n=0
while ! git pull &&  [ $n -lt 5 ]
do	
	n=$((n + 1))
	sleep 5
done
startx /usr/bin/java -jar cubo.jar -- -nocursor -s 0 2>&1 | tee logs/"$(date +%Y%m%d-%H%M%S)".out &